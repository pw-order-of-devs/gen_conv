class ApplicationController < ActionController::Base
  def index
    render template: 'layouts/application'
  end

  def version
    render json: '0.0.1'
  end
end
