class NpwzGen
  def self.npwz
    npwz = Array.new(7) { |_| rand(10) }
    checksum = npwz.each_with_index.reduce(0) { |sum, (n, i)| sum + (n * (i + 1)) } % 11
    checksum.to_s + npwz.join
  end

  def self.npwz_ph
    oia = '%02d' % (rand(20) + 1)
    serie = '%05d' % (rand(99_999) + 1)
    s = (oia.to_s + serie.to_s)
    checksum = (s[1].to_i + s[3].to_i + s[5].to_i) + 3 * (s[0].to_i + s[2].to_i + s[4].to_i + s[6].to_i)
    checksum = 10 - (checksum % 10)
    checksum = 0 if checksum == 10
    s + checksum.to_s
  end
end
