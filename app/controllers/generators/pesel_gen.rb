class PeselGen

  def self.pesel(gender, date)
    pesel = [
      date(date),
      serie_num,
      gender(gender)
    ].join
    pesel + PeselGen.checksum(pesel).to_s
  end

  def self.checksum(par = '')
    mask = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3]
    val = par.split('').map(&:to_i)
    modulo = (mask.inject(0) { |sum, num| sum + (num * val.shift) } % 10)
    modulo.zero? ? 0 : 10 - modulo
  end

  def self.serie_num
    '%03d' % rand(0..999)
  end

  def self.gender(par = '')
    g = rand(1..2) if par.nil? || par.to_i.zero?
    if g == 1
      (0..9).to_a.select(&:odd?).sample
    else
      (0..9).to_a.select(&:even?).sample
    end
  end

  def self.date(par = '')
    d = Date.parse(par.nil? || par.empty? ? Time.at(rand * Time.now.to_i).to_s : par)
    [d.strftime('%y'), month_offset(d), d.strftime('%d')]
  end

  def self.month_offset(date)
    offset =
      if (1800..1899).include?(date.year)
        80
      elsif (1900..1999).include?(date.year)
        0
      elsif (2000..2099).include?(date.year)
        20
      elsif (2100..2199).include?(date.year)
        40
      elsif (2200..2299).include?(date.year)
        60
      else
        raise ArgumentError, 'Year should be between 1800 and 2299'
      end
    '%02d' % (date.month + offset)
  end
end
