class NipGen

  def self.nip
    nip = Array.new(9) { |_| rand(10) }
    checksum = nip.each_with_index.reduce(0) { |sum, (n, i)| sum + (n * mult(i)) } % 11
    (nip.append checksum).join
  end

  def self.mult(index)
    case index
    when 0
      6
    when 1
      5
    when 2
      7
    else
      index - 1
    end
  end
end
