class RegonGen

  def self.regon(mode)
    if mode == '9'
      regon_9.join
    elsif mode == '14'
      regon_14.join
    else
      raise ArgumentError, 'Mode should be \'9\' or \'14\''
    end
  end

  def self.regon_9
    regon = Array.new(8) { |_| rand(10) }
    checksum = regon.each_with_index.reduce(0) { |sum, (n, i)| sum + (n * mult9(i)) } % 11
    regon.append checksum
  end

  def self.regon_14
    regon = regon_9
    regon = regon.concat Array.new(4) { |_| rand(10) }
    checksum = regon.each_with_index.reduce(0) { |sum, (n, i)| sum + (n * mult14(i)) } % 11
    regon.append checksum
  end

  def self.mult9(index)
    case index
    when 0
      8
    when 1
      9
    else index
    end
  end

  def self.mult14(index)
    case index
    when 0
      2
    when 1
      4
    when 2
      8
    when 3
      5
    when 4
      0
    when 5
      9
    when 6
      7
    when 7
      3
    when 8
      6
    when 9
      1
    when 10
      2
    when 11
      4
    when 12
      8
    else
      0
    end
  end
end
