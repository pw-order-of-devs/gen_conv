class GeneratorsController < ApplicationController
  require 'securerandom'
  require_relative 'generators/pesel_gen'
  require_relative 'generators/regon_gen'
  require_relative 'generators/nip_gen'
  require_relative 'generators/npwz_gen'

  def random_str
    params = request.query_parameters
    render json: SecureRandom.alphanumeric(params['length'].to_i)
  end

  def random_num
    params = request.query_parameters
    render json: SecureRandom.random_number(params['max'].to_i - params['min'].to_i + 1) + params['min'].to_i
  end

  def pesel
    params = request.query_parameters
    render json: PeselGen.pesel(params['gender'], params['date'])
  end

  def regon
    params = request.query_parameters
    render json: RegonGen.regon(params['mode'])
  end

  def nip
    render json: NipGen.nip
  end

  def npwz
    render json: NpwzGen.npwz
  end

  def npwz_ph
    render json: NpwzGen.npwz_ph
  end
end
