Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/' => 'application#index'

  get '/version', to: 'application#version'

  get '/generators/random_str', to: 'generators#random_str'
  get '/generators/random_num', to: 'generators#random_num'
  get '/generators/pesel', to: 'generators#pesel'
  get '/generators/regon', to: 'generators#regon'
  get '/generators/nip', to: 'generators#nip'
  get '/generators/npwz', to: 'generators#npwz'
  get '/generators/npwz_ph', to: 'generators#npwz_ph'
end
